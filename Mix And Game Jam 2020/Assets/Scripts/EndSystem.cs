﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSystem : MonoBehaviour
{
    [SerializeField] private PointCounter m_pointSystem;

    public void EndGame()
    {
        int accuracy = m_pointSystem.AccuracyPoint;
        int building = m_pointSystem.BuildingPoint;

        PlayerPrefs.SetInt("accuracy", accuracy);
        PlayerPrefs.SetInt("building", building);

        SceneManager.LoadScene("EndScene");
    }
}
