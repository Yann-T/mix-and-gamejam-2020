﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PointCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_totalPointsLabel;
    [SerializeField] private TextMeshProUGUI m_pointsAccuracyLabel;
    [SerializeField] private TextMeshProUGUI m_pointsBuildingLabel;
    [SerializeField] private TextMeshProUGUI m_pointsBuildingDiffLabel;
    [SerializeField] private TextMeshProUGUI m_pointsAccuracyDiffLabel;
    [SerializeField] private TextMeshProUGUI m_totalPointsDiffLabel;
    [SerializeField] private Animation m_pointsBuildingDiffanim;
    [SerializeField] private Animation m_pointsAccuracyDiffanim;
    [SerializeField] private Animation m_totalPointsDiffanim;
    [SerializeField] private GridController m_grid;

    private const string TOTAL_POINTS_LABEL_PREFIX = "Total : ";
    private const string BUILDINGS_POINTS_LABEL_PREFIX = "Buildings points : ";
    private const string ACCURACY_POINTS_LABEL_PREFIX = "Accuracy points : ";
    private int m_pointsAccuracy = 0;
    private int m_pointsBuildings = 0;

    public void Start()
    {
        m_grid.BuildingPlaced.AddListener(ExecuteOnBuildingPlaced);
    }

    public int AccuracyPoint
    {
        get => m_pointsAccuracy;
    }

    public int BuildingPoint
    {
        get => m_pointsBuildings;
    }

    private int CheckSurroundingBuilding(int xCenter, int yCenter)
    {
        int nbHouses = 0;
        int nbFactory = 0;
        int nbCommerce = 0;

        if (!m_grid.Cells[xCenter, yCenter].Building)
        {
            return 0;
        }

        for (int y = yCenter - 1; y <= yCenter + 1; y++)
        {
            for (int x = xCenter - 1; x <= xCenter + 1; x++)
            {
                if (x < 0 || y < 0 || x >= m_grid.GridBuilder.GridSize.x || y >= m_grid.GridBuilder.GridSize.y)
                {
                    continue;
                }

                if (xCenter == x && yCenter == y)
                {
                    continue;
                }
                if (m_grid.Cells[x, y].Building != null)
                {
                    if (m_grid.Cells[x, y].Building.name == "House")
                    {
                        nbHouses++;
                    }
                    if (m_grid.Cells[x, y].Building.name == "Factory")
                    {
                        nbFactory++;
                    }
                    if (m_grid.Cells[x, y].Building.name == "Commerce")
                    {
                        nbCommerce++;
                    }
                }
            }
        }
        return (int)m_grid.Cells[xCenter, yCenter].Building.GetPoints(nbHouses, nbFactory, nbCommerce);
    }

    private void ExecuteOnBuildingPlaced()
    {
        int prevPoints = m_pointsBuildings;
        m_pointsBuildings = 0;
        for (int y = 0; y < m_grid.GridBuilder.GridSize.y; y++)
        {
            for (int x = 0; x < m_grid.GridBuilder.GridSize.x; x++)
            {
                if (!m_grid.Cells[x, y].Building)
                {
                    continue;
                }
                m_pointsBuildings += CheckSurroundingBuilding(x, y);
            }
        }
        int diff = prevPoints - m_pointsBuildings;
        if (diff < 0)
        {
            m_pointsBuildingDiffLabel.text = " + " + Mathf.Abs(diff);
            m_pointsBuildingDiffLabel.color = new Color(50/255, .8f, 50 / 255, 0);
        }
        else
        {
            m_pointsBuildingDiffLabel.text = " - " + Mathf.Abs(diff);
            m_pointsBuildingDiffLabel.color = new Color(.8f, 50 / 255, 50 / 255, 0);
        }
        if (diff != 0)
        {
            m_pointsBuildingDiffanim.Play();
        }
        m_totalPointsLabel.text = TOTAL_POINTS_LABEL_PREFIX + (m_pointsBuildings + m_pointsAccuracy).ToString();
        m_pointsBuildingLabel.text = BUILDINGS_POINTS_LABEL_PREFIX + m_pointsBuildings.ToString();
    }

    public void ChangePointsAccuracy(int change)
    {
        m_pointsAccuracy += change;
        if(change > 0)
        {
            m_pointsAccuracyDiffLabel.text = " + " + change;
            m_pointsAccuracyDiffLabel.color = new Color(50 / 255, .8f, 50 / 255, 0);
            
        } else
        {
            m_pointsAccuracyDiffLabel.text = " - " + Mathf.Abs(change);
            m_pointsAccuracyDiffLabel.color = new Color(.8f, 50 / 255, 50 / 255, 0);
        }
        if (change != 0)
        {
            m_pointsAccuracyDiffanim.Play();
        }
        m_pointsAccuracyLabel.text = ACCURACY_POINTS_LABEL_PREFIX + m_pointsAccuracy.ToString();
        m_totalPointsLabel.text = TOTAL_POINTS_LABEL_PREFIX + (m_pointsBuildings + m_pointsAccuracy).ToString();
    }
}
