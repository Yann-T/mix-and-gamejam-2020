﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighScoreDisplay : MonoBehaviour
{
    private const string PREFIX_TEXT_HIGHTSCORE = "Highscore : ";
    private TextMeshProUGUI m_text;

    void Start()
    {
        m_text = GetComponent<TextMeshProUGUI>();
        m_text.text = PREFIX_TEXT_HIGHTSCORE + PlayerPrefs.GetInt("highscore", 0);
    }
}
