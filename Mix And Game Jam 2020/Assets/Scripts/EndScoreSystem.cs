﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScoreSystem : MonoBehaviour
{
    [SerializeField] private Text m_highscoreText;
    [SerializeField] private Text m_accuracyText;
    [SerializeField] private Text m_buildingText;
    [SerializeField] private Text m_totalText;
    [SerializeField] private Image m_titleImage;
    [SerializeField] private Sprite m_tryAgain;
    [SerializeField] private Sprite m_newHighscore;

    void Start()
    {
        int highscore = 0;

        if(PlayerPrefs.HasKey("highscore"))
        {
            highscore = PlayerPrefs.GetInt("highscore");
        }

        int accuracy = PlayerPrefs.GetInt("accuracy");
        int building = PlayerPrefs.GetInt("building");
        int total = accuracy + building;

        if (highscore < total)
        {
            m_titleImage.sprite = m_newHighscore;
            PlayerPrefs.SetInt("highscore", total);
            m_highscoreText.text = "Highscore : " + total;
        } 
        else
        {
            m_titleImage.sprite = m_tryAgain;
            m_highscoreText.text = "Highscore : " + highscore;
        }

        m_accuracyText.text = "Accuracy : " + accuracy;
        m_buildingText.text = "Building : " + building;
        m_totalText.text = "Total : " + total;
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
