﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vector3 = UnityEngine.Vector3;

public class BuildingQueue : MonoBehaviour
{
    [SerializeField] private BeatTimer m_beatTimer = null;
    [SerializeField] private uint m_displaySize = 5;
    [SerializeField] private GameObject m_spriteObject;
    [SerializeField] private SO_Building m_house;
    [SerializeField] private SO_Building m_factory;
    [SerializeField] private SO_Building m_commerce;
    [SerializeField] private SO_Building m_nothing;
    [SerializeField] private Image m_cursor;

    private RectTransform m_rectTransform;
    private Vector3 m_origin;
    private Queue<Image> m_spriteImages;
    private Queue<SO_Building> m_buildingQueue;
    private SO_Building m_currentBuilding;

    public SO_Building CurrentBuilding => m_currentBuilding;

    private void Awake()
    {
        m_rectTransform = GetComponent<RectTransform>();
        m_spriteImages = new Queue<Image>();
        m_buildingQueue = new Queue<SO_Building>();
    }
    
    void Start()
    {
        m_origin = new Vector3(m_rectTransform.offsetMin.x, transform.position.y, transform.position.z);
        m_beatTimer.BeatEvent.AddListener(OnBeat);
        m_beatTimer.HalfBeatEvent.AddListener(OnHalfBeat);
        for (int idx = 0; idx < m_displaySize; idx++)
        {
            Image image = Instantiate(m_spriteObject, transform).GetComponent<Image>();
            image.sprite = m_nothing.Sprite;
            image.transform.position = m_origin;
            image.transform.Translate(m_rectTransform.rect.width * idx / m_displaySize * Vector3.right);
            m_spriteImages.Enqueue(image);
            m_buildingQueue.Enqueue(m_nothing);
        }
        m_currentBuilding = m_nothing;
    }
    
    void Update()
    {
        m_cursor.transform.position = m_origin;
        m_cursor.transform.Translate(m_rectTransform.rect.width * (m_displaySize-1) / m_displaySize * Vector3.right);
        foreach (var img in m_spriteImages.ToArray())
        {
            float speed = Time.deltaTime * m_rectTransform.rect.width  / m_displaySize / m_beatTimer.SecondPerBeat;
            img.transform.Translate(speed*Vector3.right);
        }
    }

    private void OnBeat()
    {
        Image image = m_spriteImages.Dequeue();
        m_buildingQueue.Dequeue();
        
        SO_Building nextBuilding = GetNext();
        image.sprite = nextBuilding.Sprite;
        image.transform.position = m_origin;
        image.CrossFadeColor(Color.white, 0f, false, false);
        image.CrossFadeAlpha(1f, m_beatTimer.SecondPerBeat/4f, false);
        
        m_buildingQueue.Enqueue(nextBuilding);
        m_spriteImages.Enqueue(image);

        m_spriteImages.Peek().CrossFadeAlpha(0f, m_beatTimer.SecondPerBeat, false);
    }

    private void OnHalfBeat()
    {
        m_currentBuilding = m_buildingQueue.ToArray()[1];
    }

    private SO_Building GetNext()
    {
        switch (UnityEngine.Random.Range(0,5))
        {
            case 0:
                return m_house;
            case 1:
                return m_commerce;
            case 2:
                return m_factory;
            default:
                return m_nothing;
        }
    }
    
    
}
