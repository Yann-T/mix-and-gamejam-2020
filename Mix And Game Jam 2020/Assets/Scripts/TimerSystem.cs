﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSystem : MonoBehaviour
{
    [SerializeField] private BeatTimer m_beatTimer;
    [SerializeField] private AudioSource m_audio;
    [SerializeField] private Slider m_timerSlider;
    [SerializeField] private EndSystem m_endSystem;

    private float m_clipLength;
    private float m_clipDuration = 0;
    private float m_clipLast;

    // Start is called before the first frame update
    void Start()
    {
        m_clipLength = m_audio.clip.length;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        m_clipDuration = m_audio.time;
        m_clipLast = m_clipLength - m_audio.time;
        m_timerSlider.value = m_clipDuration / m_clipLength;

        if (m_clipLast < 0.1f)
        {
            m_beatTimer.StopTimer();
            m_endSystem.EndGame();
        }
    }
}

