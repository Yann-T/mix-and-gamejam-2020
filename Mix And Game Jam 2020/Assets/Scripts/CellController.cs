﻿using UnityEngine;

public class CellController : MonoBehaviour
{
    [SerializeField] private Transform m_buildingParent;
    [SerializeField] private Material m_defaultMaterial;
    [SerializeField] private Material m_selectableMaterial;
    [SerializeField] private Material m_selectedMaterial;

    private MeshRenderer m_meshRenderer;
    private GridController m_gridController;
    private SO_Building m_building;
    private bool m_selectable;
    private ParticleSystem m_perfectParticle;
    private ParticleSystem m_okParticle;

    public SO_Building Building { get => m_building; private set => m_building = value; }

    private void Awake()
    {
        m_gridController = GetComponentInParent<GridController>();
        m_meshRenderer = GetComponentInChildren<MeshRenderer>();
        m_perfectParticle = GetComponentsInChildren<ParticleSystem>()[0];
        m_okParticle = GetComponentsInChildren<ParticleSystem>()[1];
    }

    public bool HaveBuildingInside()
    {
        return m_buildingParent.childCount > 0;
    }

    public bool AddBuildingInside(SO_Building building, bool perfect, bool noParticle = false)
    {
        if(HaveBuildingInside())
        {
            return false;
        }
        Building = building;
        if (building.Model)
        {
            float[] possibleRotations = {0, 90, 180, 270};
            Instantiate(building.Model, transform.position,
                Quaternion.Euler(0, possibleRotations[Random.Range(0, possibleRotations.Length - 1)], 0),
                m_buildingParent);
            if (!noParticle)
            {
                if (perfect)
                {
                    m_perfectParticle.Play();
                }
                else
                {
                    m_okParticle.Play();
                }
            }
            return true;
        }
        return false;
    }

    public bool CheckSelectable()
    {
        return m_selectable;
    }

    public void SetSelectable(bool selectable)
    {
        m_selectable = selectable;
        if (selectable || HaveBuildingInside())
        {
            m_meshRenderer.material = m_selectableMaterial;
        } else
        {
            m_meshRenderer.material = m_defaultMaterial;
        }
    }

    public void SetSelected(bool selected)
    {
        if (selected)
        {
            m_meshRenderer.material = m_selectedMaterial;
        }
        else
        {
            m_meshRenderer.material = m_selectableMaterial;
        }
    }
}
