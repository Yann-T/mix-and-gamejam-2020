﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GridController : MonoBehaviour
{
    [SerializeField] private SO_Building m_house;
    [SerializeField] private SO_Building m_factory;
    [SerializeField] private SO_Building m_commerce;
    [SerializeField] private BeatTimer m_beatTimer;
    [SerializeField] private bool m_buildAdjacent = true;
    [SerializeField] private BuildingQueue m_buildingQueue = null;
    [SerializeField] private PointCounter m_pointCounter;
    [Range(-20, 0)] [SerializeField] private int m_pointsPerMiss = -10;
    [Range(0, 20)] [SerializeField] private int m_pointsPerOK = 5;
    [Range(0, 20)] [SerializeField] private int m_pointsPerPerfect = 3;
    [Range(0, .2f)] [SerializeField] private float m_perfectMaxDelay= .1f;
    [Range(0, .4f)] [SerializeField] private float m_okMaxDelay = .2f;

    [SerializeField] private Animation m_missAnim;
    [SerializeField] private Animation m_okAnim;
    [SerializeField] private Animation m_perfectAnim;

    private CellController m_selectedCell;
    private UnityEvent m_buildingPlaced = new UnityEvent();
    private GridBuilder m_gridBuilder;
    private CellController[,] m_cells;
    private List<Tuple<int, int>> m_validPositions;
    private float prevOffset = float.MaxValue;
    private bool hasHandledBeat = false;

    public CellController[,] Cells { get => m_cells; private set => m_cells = value; }
    public GridBuilder GridBuilder { get => m_gridBuilder; private set => m_gridBuilder = value; }
    public UnityEvent BuildingPlaced { get => m_buildingPlaced; private set => m_buildingPlaced = value; }

    private void Awake()
    {
        GridBuilder = GetComponent<GridBuilder>();
    }

    private void BuildCellControllersArray()
    {
        Cells = new CellController[(int)GridBuilder.GridSize.x, (int)GridBuilder.GridSize.y];
        for (int y = 0; y < GridBuilder.GridSize.y; y++)
        {
            for (int x = 0; x < GridBuilder.GridSize.x; x++)
            {
                Cells[x, y] = GridBuilder.Cells[x, y].GetComponent<CellController>();
            }
        }
    }

    void Start()
    {
        BuildCellControllersArray();
        Cells[5, 5].AddBuildingInside(m_house, false, true);
        BuildingPlaced.Invoke();
        m_validPositions = GetValidPositions();
    }

    void Update()
    {
        if (prevOffset > m_beatTimer.OffsetFromNearestBeat())
        {
            hasHandledBeat = false;
        }
        prevOffset = m_beatTimer.OffsetFromNearestBeat();

        DisplayHoveredCell();
        if (Input.GetMouseButtonDown(0))
        {
            if (m_buildingQueue.CurrentBuilding.Model)
            {
                HandleClickedCell();
            } else
            {
                m_pointCounter.ChangePointsAccuracy(m_pointsPerMiss);
                m_missAnim.Play();
                hasHandledBeat = true;
            }
        }

        if(!hasHandledBeat && m_beatTimer.OffsetFromNearestBeat() > m_okMaxDelay && m_buildingQueue.CurrentBuilding.Model)
        {
            m_pointCounter.ChangePointsAccuracy(m_pointsPerMiss);
            m_missAnim.Play();
            hasHandledBeat = true;
        }
    }

    private void DisplayHoveredCell()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            CellController cellController = objectHit.gameObject.GetComponentInParent<CellController>();
            if (m_selectedCell != null)
            {
                m_selectedCell.SetSelected(false);
            }
            if (cellController != null && cellController.CheckSelectable())
            {
                cellController.SetSelected(true);
                m_selectedCell = cellController;
            }
        }
    }

    private void HandleClickedCell()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            CellController cellController = objectHit.gameObject.GetComponentInParent<CellController>();
            if(cellController != null && cellController.CheckSelectable())
            {
                if (!hasHandledBeat)
                {
                    if (Mathf.Abs(m_beatTimer.OffsetFromNearestBeat()) < m_okMaxDelay)
                    {
                        bool perfect = false;
                        if (Mathf.Abs(m_beatTimer.OffsetFromNearestBeat()) > m_perfectMaxDelay)
                        {
                            m_pointCounter.ChangePointsAccuracy(m_pointsPerOK);
                            m_okAnim.Play();
                        }
                        else
                        {
                            m_pointCounter.ChangePointsAccuracy(m_pointsPerPerfect);
                            m_perfectAnim.Play();
                            perfect = true;
                        }
                        cellController.AddBuildingInside(m_buildingQueue.CurrentBuilding, perfect);
                        if (m_buildingQueue.CurrentBuilding.Model) {
                            BuildingPlaced.Invoke();
                        }
                        m_validPositions = GetValidPositions();
                    } else
                    {
                        m_pointCounter.ChangePointsAccuracy(m_pointsPerMiss);
                        m_missAnim.Play();
                    }
                    hasHandledBeat = true;
                }
            }
        }
    }

    private List<Tuple<int, int>> GetValidPositions()
    {
        List<Tuple<int, int>> positions = new List<Tuple<int, int>>();
        for (int y = 0; y < GridBuilder.GridSize.y; y++)
        {
            for (int x = 0; x < GridBuilder.GridSize.x; x++)
            {
                if (IsCellValid(x, y))
                {
                    Cells[x, y].SetSelectable(true);
                    positions.Add(new Tuple<int, int>(x, y));
                } else
                {
                    Cells[x, y].SetSelectable(false);
                }
            }
        }
        return positions;
    }

    private bool IsCellValid(int xCell, int yCell)
    {
        if (Cells[xCell, yCell].HaveBuildingInside())
        {
            return false;
        }
        for (int y = yCell - 1; y <= yCell + 1; y++)
        {
            for (int x = xCell - 1; x <= xCell + 1; x++)
            {
                if(x < 0 || x >= GridBuilder.GridSize.x || y < 0 || y >= GridBuilder.GridSize.y)
                {
                    continue;
                }

                if(m_buildAdjacent && Math.Abs(x - xCell) + Math.Abs(y - yCell) >= 2)
                {
                    continue;
                }

                if (x == xCell && y == yCell)
                {
                    continue;
                }

                if (Cells[x, y].HaveBuildingInside())
                {
                    return true;
                }
            }
        }
        return false;
    }
}
