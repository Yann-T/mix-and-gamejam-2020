﻿using UnityEngine.UI;
using UnityEngine;
using System;

public class MoneySystem : MonoBehaviour
{
    [SerializeField] private BeatTimer m_beatTimer;
    [SerializeField] private Text m_moneyLabel;
    [SerializeField] private Text m_happinessLabel;
    [Range(0, 10)] [SerializeField] private int m_moneyOverTimeFactory = 2;

    [SerializeField] private GridController m_grid;


    private int m_money = 100;
    private int m_happiness = 100;

    /*private Tuple<int, int> CheckSurroundingBuilding(int xCenter, int yCenter)
    {
        int nbHouses = 0;
        int nbFactory = 0;
        int nbCommerce = 0;

        if(!m_grid.Cells[xCenter, yCenter].Building)
        {
            return new Tuple<int, int>(0, 0);
        }

        for (int y = yCenter - 1; y <= yCenter + 1; y++)
        {
            for (int x = xCenter - 1; x <= xCenter + 1; x++)
            {
                if(x < 0 || y < 0 || x >= m_grid.GridBuilder.GridSize.x || y >= m_grid.GridBuilder.GridSize.y)
                {
                    continue;
                }

                if(xCenter == x && yCenter == y)
                {
                    continue;
                }
                if (m_grid.Cells[x, y].Building != null)
                {
                    if (m_grid.Cells[x, y].Building.name == "House")
                    {
                        nbHouses++;
                    }
                    if (m_grid.Cells[x, y].Building.name == "Factory")
                    {
                        nbFactory++;
                    }
                    if (m_grid.Cells[x, y].Building.name == "Commerce")
                    {
                        nbCommerce++;
                    }
                }
            }
        }
        return new Tuple<int, int>( (int) m_grid.Cells[xCenter, yCenter].Building.GetHappinessGenerated(nbHouses, nbFactory, nbCommerce),
                                    (int) m_grid.Cells[xCenter, yCenter].Building.GetMoneyGenerated(nbHouses, nbFactory, nbCommerce));
    }*/

    private void ExecuteOnBeat()
    {
        /*for (int y = 0; y < m_grid.GridBuilder.GridSize.y; y++)
        {
            for (int x = 0; x < m_grid.GridBuilder.GridSize.x; x++)
            {
                if (!m_grid.Cells[x, y].Building)
                {
                    continue;
                }
                Tuple<int, int> changes = CheckSurroundingBuilding(x, y);
                m_happiness += changes.Item1;
                m_money += changes.Item2;
                if (m_grid.Cells[x, y].Building.name == "Factory")
                {
                    m_money += m_moneyOverTimeFactory;
                }
            }
        }

        m_moneyLabel.text = "" + m_money;
        m_happinessLabel.text = m_happiness + "%";*/
    }

    public void RemoveMoney(int amount)
    {
        m_money -= amount;
    }

    public void Start()
    {
        m_beatTimer.BeatEvent.AddListener(ExecuteOnBeat);
    }

    public double Money
    {
        get { return m_money; }
    }


}
