﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.Events;

public class BeatTimer : MonoBehaviour
{
    [SerializeField] private float m_bpm = 120f;
    private float m_beatPerSecond;
    private float m_currentBeat;
    private float m_currentHalfBeat;

    private bool m_started = false;
    
    private UnityEvent m_beatEvent;
    private UnityEvent m_halfBeatEvent;

    [SerializeField] private AudioSource m_audioSource = null;

    public float SecondPerBeat => 1f / m_beatPerSecond;
    
    public UnityEvent BeatEvent => m_beatEvent;
    public UnityEvent HalfBeatEvent => m_halfBeatEvent;
    public float BeatProgression
    {
        get => m_audioSource.time * m_beatPerSecond;
        set => m_audioSource.time = value / m_beatPerSecond;
    }

    private void Awake()
    {
        m_beatEvent = new UnityEvent();
        m_halfBeatEvent = new UnityEvent();
        m_beatPerSecond = m_bpm/60f;
    }

    public void StartTimer()
    {
        m_started = true;
        m_audioSource.Play();
    }

    public void StopTimer()
    {
        m_started = true;
        m_audioSource.Stop();
    }

    void Update()
    {
        if (m_started)
        {
            var beatProgression = BeatProgression;
            var floorBeatProgression = (float)Math.Floor(beatProgression);
            if (m_currentBeat < floorBeatProgression)
            {
                m_currentBeat = floorBeatProgression;
                m_beatEvent.Invoke();
            }
            else if (m_currentHalfBeat < Math.Floor(beatProgression - 0.5f))
            {
                m_currentHalfBeat = floorBeatProgression;
                m_halfBeatEvent.Invoke();
            }
        }
    }

    public float OffsetFromNearestBeat()
    {
        return BeatProgression - Mathf.Round(BeatProgression);
    }
}
