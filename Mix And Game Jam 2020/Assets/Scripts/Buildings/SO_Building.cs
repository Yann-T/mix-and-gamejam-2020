﻿using UnityEngine;

[CreateAssetMenu(fileName = "SO_Building", menuName = "SO/SO_Building", order = 1)]
public class SO_Building : ScriptableObject
{
    [SerializeField] private Sprite m_sprite;
    [SerializeField] private GameObject m_model;
    [Range(-2, 2)] [SerializeField] private int m_points;
    [Range(-2, 2)][SerializeField] private int m_pointsNearHouse;
    [Range(-2, 2)] [SerializeField] private int m_pointsNearFactory;
    [Range(-2, 2)] [SerializeField] private int m_pointsNearCommerce;

    public Sprite Sprite => m_sprite;
    public GameObject Model => m_model;

    public float GetPoints(int nbHouseNearby, int nbFactoryNearby, int nbCommerceNearby)
    {
        return nbHouseNearby * m_pointsNearHouse + nbFactoryNearby * m_pointsNearFactory + nbCommerceNearby * m_pointsNearCommerce;
    }
}
