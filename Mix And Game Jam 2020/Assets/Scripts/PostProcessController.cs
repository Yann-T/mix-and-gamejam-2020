﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessController : MonoBehaviour
{
    [SerializeField] private BeatTimer m_beatTimer;
    [SerializeField] private PostProcessVolume m_postProcessVolume;
    [SerializeField] private AnimationCurve m_animationCurve;
    [Range(0, 1)][SerializeField] private float m_animationDuration = .3f;
    [Range(1, 100)][SerializeField] private float m_lensDistortionIntensity = 25f;
    [Range(0, 2)] [SerializeField] private float m_chromaticAberrationIntensity = 1f;
    [Range(0, 10)] [SerializeField] private float m_bloomIntensity = 1f;

    private LensDistortion m_lensDistortion;
    private ChromaticAberration m_chromaticAberration;
    private Bloom m_bloom;
    private bool m_isPlayingAnimation = false;
    private float m_animationTime = 0f;

    void Start()
    {
        m_beatTimer.BeatEvent.AddListener(OnBeatEffect);
        m_postProcessVolume.profile.TryGetSettings(out m_lensDistortion);
        m_postProcessVolume.profile.TryGetSettings(out m_chromaticAberration);
        m_postProcessVolume.profile.TryGetSettings(out m_bloom);
    }

    void Update()
    {
        if(m_isPlayingAnimation)
        {
            m_animationTime += Time.deltaTime;
            UpdateLensDistortionEffect();
            UpdateChromaticAbberationEffect();
            UpdateBloomEffect();

            if (m_animationTime > m_animationDuration)
            {
                m_isPlayingAnimation = false;
                m_animationTime = 0f;
            }
        }
    }

    private void OnBeatEffect()
    {
        m_isPlayingAnimation = true;
    }

    private void UpdateLensDistortionEffect()
    {
        m_lensDistortion.intensity.value = Mathf.Lerp(m_lensDistortionIntensity, 1, m_animationCurve.Evaluate(m_animationTime/ m_animationDuration));
    }

    private void UpdateChromaticAbberationEffect()
    {
        m_chromaticAberration.intensity.value = Mathf.Lerp(m_chromaticAberrationIntensity, 0, m_animationCurve.Evaluate(m_animationTime / m_animationDuration));
    }

    private void UpdateBloomEffect()
    {
        m_bloom.intensity.value = Mathf.Lerp(m_bloomIntensity, 0, m_animationCurve.Evaluate(m_animationTime / m_animationDuration));
    }
}
