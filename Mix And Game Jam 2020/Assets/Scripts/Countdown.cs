﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    private int m_countdown = 3;

    [SerializeField] private Image m_countdownImage;
    [SerializeField] private Animation m_countdownAnimation;
    [SerializeField] private BeatTimer m_beatTimer;

    [SerializeField] private Sprite m_3Sprite;
    [SerializeField] private Sprite m_2Sprite;
    [SerializeField] private Sprite m_1Sprite;
    [SerializeField] private Sprite m_goSprite;

    [SerializeField] private AudioClip m_digitCountdownSound;
    [SerializeField] private AudioClip m_goCountdownSound;

    private AudioSource m_audioSource;

    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CountdownToGo());
    }

    IEnumerator CountdownToGo()
    {
        while (m_countdown > 0)
        {
            m_audioSource.clip = m_digitCountdownSound;
            m_audioSource.Play();
            switch(m_countdown)
            {
                case 3:
                    m_countdownImage.sprite = m_3Sprite;
                    m_countdownAnimation.Play();
                    break;
                case 2:
                    m_countdownImage.sprite = m_2Sprite;
                    m_countdownAnimation.Play();
                    break;
                case 1:
                    m_countdownImage.sprite = m_1Sprite;
                    m_countdownAnimation.Play();
                    break;
            }

            yield return new WaitForSeconds(1f);

            m_countdown--;
        }

        m_audioSource.clip = m_goCountdownSound;
        m_audioSource.Play();
        m_countdownImage.sprite = m_goSprite;
        m_countdownAnimation.Play();

        yield return new WaitForSeconds(1f);

        m_beatTimer.StartTimer();
    }
}
