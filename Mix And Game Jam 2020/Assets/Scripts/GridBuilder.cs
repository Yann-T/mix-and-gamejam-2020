﻿using UnityEngine;

public class GridBuilder : MonoBehaviour
{
    [SerializeField] private GameObject m_cellObject;
    [SerializeField] private Vector2 m_centerOffset = Vector2.zero;
    [SerializeField] private Vector2 m_gridSpacing = new Vector2(2, 2);
    [SerializeField] private Vector2 m_gridSize = new Vector2(10, 10);

    public GameObject[,] Cells { get; private set; }
    public Vector2 GridSize { get => m_gridSize; private set => m_gridSize = value; }
    public Vector2 GridSpacing { get => m_gridSpacing; private set => m_gridSpacing = value; }
    public Vector2 CenterOffset { get => m_centerOffset; private set => m_centerOffset = value; }

    private void Awake()
    {
        Cells = new GameObject[(int)GridSize.x, (int)GridSize.y];
        foreach (Transform transform in transform)
        {
            Destroy(transform.gameObject);
        }

        for (int y = 0; y < GridSize.y; y++)
        {
            for (int x = 0; x < GridSize.x; x++)
            {
                Vector3 cellPos = CenterOffset;
                cellPos.x += GridSpacing.x * x + transform.position.x - GridSize.x / 2 * GridSpacing.x + GridSpacing.x / 2;
                cellPos.z += GridSpacing.y * y + transform.position.z - GridSize.y / 2 * GridSpacing.y + GridSpacing.y / 2;
                Cells[x, y] = Instantiate(m_cellObject, cellPos, Quaternion.identity, transform);
            }
        }
    }

    private void Start()
    {
        
    }

}
